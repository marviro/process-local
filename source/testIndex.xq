xquery version "3.1";

declare namespace functx = "http://www.functx.com";
declare namespace html = "http://www.w3.org/1999/xhtml";

(: ##### ouvre la base XML ####:)
(: let $chapitres := db:open("lankes7")/html/body/article :) 

(: ##### ouvre un document HTML #### :)
(: quand le script exec sera en mesure de traiter tous les chapitres à la fois, 
il faudra utiliser une "collection", voir https://www.abbeyworkshop.com/howto/xml/xql-saxon-basics/ :)
let $chapitres := fn:doc("/home/nicolas/Documents/wget/lankes7/chapitre7id.html")/html:html/html:body/html:article


let $mapOrganisme := map:merge(
  for $occurence in $chapitres//*[@class='organisme']
    let $chapitre := fn:substring-after(fn:base-uri($occurence),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $mapLieu := map:merge(
  for $occurence in $chapitres//*[@class='lieu']
    let $chapitre := fn:substring-after(fn:base-uri($occurence),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $mapPersonnalite := map:merge(
  for $occurence in $chapitres//*[@class='personnalite']
    let $chapitre := fn:substring-after(fn:base-uri($occurence),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $mapSite := map:merge(
  for $occurence in $chapitres//*[@class='site']
    let $chapitre := fn:substring-after(fn:base-uri($occurence),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $allOrganisme := 
  <div class="index"><h2>Organismes</h2>{
    map:for-each($mapOrganisme, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          let $link := <a href="{$value}">lien</a>
          return $link 
      }</p>
      return $occurence
    }    
    )
  }    
  </div>

let $allLieu := 
  <div class="index"><h2>Lieux</h2>{
    map:for-each($mapLieu, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          let $link := <a href="{$value}">lien</a>
          return $link 
      }</p>
      return $occurence
    }    
    )
  }    
  </div>
  
let $allPersonnalite := 
  <div class="index"><h2>Personnalités</h2>{
    map:for-each($mapPersonnalite, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          let $link := <a href="{$value}">lien</a>
          return $link 
       }</p>
      return $occurence
    }    
    )
  }    
  </div>

let $allSite := 
  <div class="index"><h2>Sites</h2>{
    map:for-each($mapSite, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          let $link := <a href="{$value}">lien</a>
          return $link 
      }</p>
      return $occurence
    }    
    )
  }    
  </div>

(: return <div class="index">{$allOrganisme} {$allLieu}  {$allPersonnalite} {$allSite} </div> :) 

let $index := 
	<div class="index">{$allOrganisme} {$allLieu}  {$allPersonnalite} {$allSite} </div>

return file:write('/home/nicolas/gitlab/process-local-lankes/index.html', $index, map { 'method' : 'xml', 'indent' : 'yes', 'omit-xml-declaration' : 'no'})

