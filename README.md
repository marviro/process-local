# process-local
Docker pour lancer le script de production des livres des ateliers de sens public.
Pour lancer: 

1. changer la variable au début du script source/exec.sh (avec le nom du chapitre à produire
2. Mettre les fichiers source dans /input/
3. lancer docker `sudo docker-compose -f docker-compose.yaml up --build`

Les fichiers html se trouveront dans output/


pour pandoc-crossref, voir les releases de la source utilisée : https://github.com/lierdakil/pandoc-crossref/releases


## TODO
1. générer colonne de gauche avec toc (prendre celle de metaontologie et ajouter au template)+infolivre (chercher yaml dans sitepod et générer 1 fichier metadata + 1 fichier colonne + 1 fichier page présentation livre

Pour info livre cf garde livre.md et livre.yaml (ln le merge avec sitepod)

## Installation 

1. cloner le repo
2. modifier éventuellement le dockerfile pour installer les softs et versions requises
3. modifier le script ./source/exec.sh pour effectuer les commandes requises
4. ajouter des sources à traiter
5. `docker-compose build`
6. `docker-compose run process-local` pour exécuter le script exec.sh.
