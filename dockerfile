FROM ubuntu
ENV TZ=Europe/Minsk
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install -y zip unzip wget default-jre lua5.2 python-pip haskell-doc haskell-stack r-base graphviz asymptote
COPY ./vendor /usr/local/vendor/
RUN dpkg -i /usr/local/vendor/pandoc-2.4-1-amd64.deb
RUN tar -xzf /usr/local/vendor/linux-ghc86-pandoc24.tar.gz && \
    mv pandoc-crossref /usr/bin/ && \
    pip install pandocfilters && \
    apt-get clean -y
RUN wget -qO- https://get.haskellstack.org/ | sh -s - -f
RUN stack update && stack upgrade
RUN cd /usr/local/vendor/pandoc-sidenote && stack build && stack install --verbose
RUN cp /root/.local/bin/* /bin/
RUN apt-get update --fix-missing
RUN apt-get install -y software-properties-common 
RUN add-apt-repository ppa:rmescandon/yq 
RUN apt-get update && apt-get install yq
